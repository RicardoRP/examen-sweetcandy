from django.shortcuts import render

from django.contrib.auth.models import User,Group
from rest_framework import viewsets
from rest_framework import permissions
from Api.serializador import usuarioSerializado,grupoSerializado

class vistaUsuario(viewsets.ModelViewSet):
    """
    Api endpoint permite visualizar los datos del usuario consultado
    """
    queryset = User.objects.all()
    serializer_class = usuarioSerializado
    permission_classes = [permissions.IsAuthenticated]

class vistaGrupo(viewsets.ModelViewSet):
    """
    Api endpoint permite visualizar el grupo consultado
    """
    queryset = Group.objects.all()
    serializer_class = grupoSerializado
    permission_classes = [permissions.IsAuthenticated]

