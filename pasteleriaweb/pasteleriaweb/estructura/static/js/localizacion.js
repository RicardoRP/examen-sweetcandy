class Localizacion{
    constructor(callback){
        if(navigator.geolocation){
            //obtenemos mi ubicacion
            navigator.geolocation.getCurrentPosition((position)=>{
                this.latitude = position.coords.latitude;
                this.longitud = position.coords.longitude;
            
                callback();
            });
        }else{
            // un alert q no existe o tu navegador no soporta geolozacion
            alert("Tu navegador no soporta geolocalizacion");
        }
    }
}