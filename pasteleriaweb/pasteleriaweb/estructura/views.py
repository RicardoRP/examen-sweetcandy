from django.shortcuts import render, HttpResponse, redirect

from estructura.models import Producto , DatosCliente

from django.db.models import Q

# importaciones para realizar el login : 
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from django.contrib.auth import authenticate, login , logout
from django.contrib.auth.decorators import login_required

# Create your views here.

def index(request):
    return render(request,'indexs/index.html', {'title':'Mi Inicio'})

def comprar(request):
    return render(request,'indexs/Comprar.html',{'title':'Mi tienda'})

def dato_cliente(request):

    if request.method == 'POST':
        email = request.POST.get('email')
        telefono = request.POST.get('telefono')
    
        datoscliente = DatosCliente(
            email=email,
            telefono=telefono
        )

        datoscliente.save()

        return redirect('Tienda')

    return render(request,'indexs/Cliente.html')   

def locacion(request):
    return render(request,'indexs/Location.html', {'title':'Mi locacion'})

def set1(request):
    return render(request,'indexs/Set1.html')

def set2(request):
    return render(request,'indexs/set2.html')

def set3(request):
    return render(request,'indexs/set3.html')

def set4(request):
    return render(request,'indexs/set4.html')

def set5(request):
    return render(request,'indexs/set5.html')

def set6(request):
    return render(request,'indexs/set6.html')

def set7(request):
    return render(request,'indexs/set7.html')

def set8(request):
    return render(request,'indexs/set8.html')

def set9(request):
    return render(request,'indexs/set9.html')

def set10(request):
    return render(request,'indexs/set10.html')

def set11(request):
    return render(request,'indexs/set11.html')

def set12(request):
    return render(request,'indexs/set12.html')


# Funcion Registro de usuario : 
def registro(request):
    if request.user.is_authenticated:
        return redirect('index')

    else :    
        formularioRegistros = UserCreationForm()

        if request.method == 'POST':
            formularioRegistros = UserCreationForm(request.POST)

            if formularioRegistros.is_valid:
                formularioRegistros.save()

                messages.success(request,'Registro Exitoso !!')

                return redirect('login')

    return render(request,'Usuarios/registro.html',{'formularioRegistros':formularioRegistros})

# Funcion Login : 
def user_login(request):

    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username , password=password)

        if user is not None : 

            login(request, user)

            return redirect('MainAdmin')

        else:
            messages.warning(request,'Identifícación Incorrecta!!! Si no tiene cuenta registrese...')    

    return render(request,'Usuarios/login.html')

def recuperar(request):
    if request.user.is_authenticated:
        return redirect('index')

    else :    
        formularioRegistros = UserCreationForm()

        if request.method == 'POST':
            formularioRegistros = UserCreationForm(request.POST)

            if formularioRegistros.is_valid:
                formularioRegistros.save()

                messages.success(request,'Registro Exitoso !!')

                return redirect('login')

    return render(request,'Usuarios/recuperar.html',{'formularioRegistros':formularioRegistros})    
def envio(request):
    if request.user.is_authenticated:
        return redirect('index')

    else :    
        formularioRegistros = UserCreationForm()

        if request.method == 'POST':
            formularioRegistros = UserCreationForm(request.POST)

            if formularioRegistros.is_valid:
                formularioRegistros.save()

                messages.success(request,'Registro Exitoso !!')

                return redirect('login')

    return render(request,'Usuarios/envio.html',{'formularioRegistros':formularioRegistros})    

# Funcion Logout :
@login_required(login_url="login") 
def Logout_user(request):
    logout(request)
    return redirect('index')

# --------- . -----------
@login_required(login_url="login")
def mainAdmin(request):
    return render(request,'Admin/MainAdmin.html')

# Funcion AGREGAR productos : 
@login_required(login_url="login")
def Ingresar(request):
    
    if request.method == 'POST':
        nombre = request.POST.get('nameproducto')
        precio = request.POST.get('precio')
        descripcion = request.POST.get('descripcion')
        cantidad = request.POST.get('cantidad')
    
        producto = Producto(
            nombre=nombre,
            precio=precio,
            descripcion=descripcion,
            cantidad=cantidad
        )

        producto.save()

        messages.success(request,'Registro Exitoso !!!')
    
    # else:
    #     messages.warning(request,'Error con guardar!!!')

    return render(request,'Admin/Ingresar.html')

# Funcion EDTAR productos : 
@login_required(login_url="login")
def Modificar(request):

    if request.method == 'POST':
        id = request.POST.get('id')

        producto = Producto.objects.get(id = id)

        producto.nombre = request.POST.get('nameproducto')
        producto.precio = request.POST.get('precio')
        producto.descripcion = request.POST.get('descripcion')
        producto.cantidad = request.POST.get('cantidad')

        producto.save()
    
        messages.success(request,f'Producto actualizado : {producto.nombre}')

    return render(request,'Admin/Modificar.html')

# Funcion Eliminar productos : 
@login_required(login_url="login")
def Eliminar(request):

    if request.method == 'POST':
        id = request.POST.get('id')

        producto = Producto.objects.get(pk = id)

        producto.delete()

        messages.success(request,'Eliminado Correctamente !!!')

    return render(request,'Admin/Eliminar.html')

# Funcion Listar Productos : 
@login_required(login_url="login")
def Listar(request):

    productos = Producto.objects.all()

    return render(request,'Admin/Listar.html',{'productos':productos}) 


