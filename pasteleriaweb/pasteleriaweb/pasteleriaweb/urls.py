"""pasteleriaweb URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.db import router
from django.urls import path
import rest_framework
import estructura.views
import ApiSocial.views
import AppPWA.views

#Clases para ApiSocial
from django.views.generic import TemplateView

#Clases para la url de la Api
from django.urls import include
from rest_framework import routers
import Api.views

router = routers.DefaultRouter()
router.register(r'users', Api.views.vistaUsuario)
router.register(r'users', Api.views.vistaGrupo)

urlpatterns = [
    path('admin/', admin.site.urls),
    # MIS RUTAS 
    path('', estructura.views.index, name="index"),
    path('Tienda/', estructura.views.comprar, name="Tienda" ),
    path('Tienda/datoCliente/', estructura.views.dato_cliente, name="datoCliente"),
    path('Locacion/', estructura.views.locacion, name="Locacion"),
    path('Tienda/Set1/', estructura.views.set1, name="Set1"),
    path('Tienda/Set2/', estructura.views.set2, name="Set2"),
    path('Tienda/Set3/', estructura.views.set3, name="Set3"),
    path('Tienda/Set4/', estructura.views.set4, name="Set4"),
    path('Tienda/Set5/', estructura.views.set5, name="Set5"),
    path('Tienda/Set6/', estructura.views.set6, name="Set6"),
    path('Tienda/Set7/', estructura.views.set7, name="Set7"),
    path('Tienda/Set8/', estructura.views.set8, name="Set8"),
    path('Tienda/Set9/', estructura.views.set9, name="Set9"),
    path('Tienda/Set10/', estructura.views.set10, name="Set10"),
    path('Tienda/Set11/', estructura.views.set11, name="Set11"),
    path('Tienda/Set12/', estructura.views.set12, name="Set12"),
    #path('Login/', views.login, name="Login"),
    #path('Login/Registro/', views.registro, name="Registro"),

    # LOGIN Y REGISTRO DE USUARIO : 
    path('registro/', estructura.views.registro, name="registro"),
    path('login/',estructura.views.user_login, name="login"),
    path('logout/',estructura.views.Logout_user, name="logout"),
     path('recuperar/', estructura.views.recuperar, name="recuperar"),
    path('envio/', estructura.views.envio, name="envio"),
 
    # PAGINA ADMINISTRATIVA (CRUD): 
    path('MainAdmin/', estructura.views.mainAdmin, name="MainAdmin"),
    path('Ingresar/', estructura.views.Ingresar, name="Ingresar"),
    path('Modificar/', estructura.views.Modificar, name="Modificar"),
    path('Eliminar/', estructura.views.Eliminar, name="Eliminar"),
    path('Listar/', estructura.views.Listar, name="Listar"),

    #PATH API
    path('opcionapi/',include(router.urls)),
    path('api-auth/',include('rest_framework.urls', namespace='rest_framework')),

    #PATH APISOCIAL
    path('Home/', ApiSocial.views.Home, name="Home"),
    path('accounts/', include('allauth.urls')),

    path('loginn/',ApiSocial.views.loginn,name='loginn'),
    # path('', TemplateView.as_view(template_name='ApiSocial/index.html')),
    path('logoutt/',ApiSocial.views.logout_u, name="logoutt"),

    #Path para PWA
    path('appwa/',AppPWA.views.indexPWA, name="indexPWA"),
]
