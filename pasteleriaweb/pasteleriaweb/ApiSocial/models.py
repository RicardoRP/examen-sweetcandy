from django.db import models

# Create your models here.

class RegistroCliente(models.Model):
    email = models.CharField(max_length=150)
    contraseña = models.CharField(max_length=15)
    fechaCreacion = models.DateTimeField(auto_now_add=True)
