from django.apps import AppConfig


class ApisocialConfig(AppConfig):
    name = 'ApiSocial'
