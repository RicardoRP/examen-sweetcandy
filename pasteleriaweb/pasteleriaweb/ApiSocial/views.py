from django.shortcuts import render
from django.shortcuts import render, HttpResponse, redirect

from ApiSocial.models import RegistroCliente

from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from django.contrib.auth import authenticate, login , logout
from django.contrib.auth.decorators import login_required

# Create your views here.

def Home(request):
    return render(request,'Home.html')

def loginn(request):

    if request.method == 'POST':
        email = request.POST.get('email')
        password = request.POST.get('pass')

        user = authenticate(request, email=email ,password=password)

        if user is not None : 

            login(request, user)

            return redirect('Home')
        
        else:
            messages.warning(request,'Identifícación Incorrecta!!! Si no tiene cuenta registrese...')
            return redirect('Home')   


    return render(request,'ApiSocial/index.html')

@login_required(login_url="loginn")
def logout_u(request):
    logout(request)
    return redirect('loginn')
