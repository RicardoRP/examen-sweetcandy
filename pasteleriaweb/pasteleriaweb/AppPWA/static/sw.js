//asignar un nombre y versión al cache
const CACHE_NAME = 'v1_cache_djangopwa',
  urlsToCache = [
    '/',
    'https://fonts.googleapis.com/css?family=Raleway:400,700',
    'https://fonts.gstatic.com/s/raleway/v12/1Ptrg8zYS_SKggPNwJYtWqZPAA.woff2',
    'https://use.fontawesome.com/releases/v5.0.7/css/all.css',
    'https://use.fontawesome.com/releases/v5.0.6/webfonts/fa-brands-400.woff2',
    '/static/style.css',
    '/static/script.js',
    '/static/img/splash.png',
    '/static/img/favicon.png'
  ]

//durante la fase de instalación, generalmente se almacena en caché los activos estáticos
self.addEventListener('install', e => {
  e.waitUntil(
    caches.open(CACHE_NAME)
      .then(cache => {
        return cache.addAll(urlsToCache)
          .then(() => self.skipWaiting())
      })
      .catch(err => console.log('Falló registro de cache', err))
  )
})

//una vez que se instala el SW, se activa y busca los recursos para hacer que funcione sin conexión
self.addEventListener('activate', e => {
  const cacheWhitelist = [CACHE_NAME]

  e.waitUntil(
    caches.keys()
      .then(cacheNames => {
        return Promise.all(
          cacheNames.map(cacheName => {
            //Eliminamos lo que ya no se necesita en cache
            if (cacheWhitelist.indexOf(cacheName) === -1) {
              return caches.delete(cacheName)
            }
          })
        )
      })
      // Le indica al SW activar el cache actual
      .then(() => self.clients.claim())
  )
})

//cuando el navegador recupera una url
self.addEventListener('fetch', e => {
  //Responder ya sea con el objeto en caché o continuar y buscar la url real
  e.respondWith(
    caches.match(e.request)
      .then(res => {
        if (res) {
          //recuperar del cache
          return res
        }
        //recuperar de la petición a la url
        return fetch(e.request)
      })
  )
})

//Codigo para notificaciones
importScripts('https://www.gstatic.com/firebasejs/3.9.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/3.9.0/firebase-messaging.js');

// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyAi7_-gBc9Nw_YQ5Gbh6mdrhBJJ1DriX7A",
  authDomain: "pasteleria-e5808.firebaseapp.com",
  projectId: "pasteleria-e5808",
  storageBucket: "pasteleria-e5808.appspot.com",
  messagingSenderId: "169639267346",
  appId: "1:169639267346:web:891aa976df281907039ccb"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

// preparamos enlace con serverworker
// y solicita permiso para recepcion de notificaciones
let messaging = firebase.messaging();


messaging.setBackgroundMessageHandler(function(payload){
  let title = 'titulo de la notificacion';
  let options = {
    body: 'cuerpo del Mensaje',
    icon:'/static/img/icono.png'

  }

  self.registration.showNotification(title,options);
  
});