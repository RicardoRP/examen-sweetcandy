from django.shortcuts import render

# Create your views here.
def indexPWA(request):
    template='html/index.html'
    return render(request,template)
