from django.apps import AppConfig


class ApppwaConfig(AppConfig):
    name = 'AppPWA'
